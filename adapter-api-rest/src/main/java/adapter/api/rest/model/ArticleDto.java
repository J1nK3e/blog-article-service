package adapter.api.rest.model;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ArticleDto {
  private String id;
  @NonNull private String name;
  private String lastUpdate;
  private String dateOfCreation;
}
