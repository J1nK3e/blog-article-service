package adapter.api.rest;

import adapter.api.rest.mapper.ArticleDtoMapper;
import adapter.api.rest.mapper.ArticleMapper;
import adapter.api.rest.model.ArticleDto;
import de.geldentdecker.domain.logic.ArticleHandler;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class ArticleController {

  @Autowired private ArticleHandler articleHandler;

  @PostMapping(
      path = "/articles",
      consumes = "application/json",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Map<String, String> writeAnArticle(@RequestBody ArticleDto articleDto) {
    ArticleMapper.INSTANCE.map(articleDto);
    val enrichedArticle = articleHandler.buildArticle(ArticleMapper.INSTANCE.map(articleDto));
    val savedArticle = articleHandler.saveArticle(enrichedArticle);
    val response = new HashMap<String, String>();
    response.put("id", savedArticle.getId());
    return response;
  }

  @GetMapping(path = "/articles", produces = MediaType.APPLICATION_JSON_VALUE)
  public Set<ArticleDto> readAllArticles() {
    val response = articleHandler.readAllArticles();
    return response.stream().map(ArticleDtoMapper.INSTANCE::map).collect(Collectors.toSet());
  }
}
