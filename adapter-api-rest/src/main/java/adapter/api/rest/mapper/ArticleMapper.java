package adapter.api.rest.mapper;

import adapter.api.rest.model.ArticleDto;
import de.geldentdecker.domain.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class ArticleMapper {

  public static final ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);

  @Mapping(target = "id", ignore = true)
  @Mapping(target = "lastUpdate", ignore = true)
  @Mapping(target = "dateOfCreation", ignore = true)
  public abstract Article map(ArticleDto articleDto);
}
