package adapter.api.rest.mapper;

import adapter.api.rest.model.ArticleDto;
import de.geldentdecker.domain.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class ArticleDtoMapper {

  public static final ArticleDtoMapper INSTANCE = Mappers.getMapper(ArticleDtoMapper.class);

  public abstract ArticleDto map(Article article);
}
