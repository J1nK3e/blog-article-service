package adapter.api.rest.mapper;

import adapter.persistence.mongodb.mapper.ArticleMapper;
import adapter.persistence.mongodb.model.ArticleForDatabase;
import lombok.val;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArticleMapperTest {

  private static final String ARTICLE_NAME = "ArticleName";

  @Test
  public void testMappingFromArticleDtoToArticle() {

    // given
    val articleDto = ArticleForDatabase.builder().name(ARTICLE_NAME).build();

    // when
    val mappedArticleDto = ArticleMapper.INSTANCE.map(articleDto);

    // then
    assertNotNull(mappedArticleDto);
    assertNotNull(mappedArticleDto.getName());
    assertEquals(mappedArticleDto.getName(), ARTICLE_NAME);
    assertNull(mappedArticleDto.getLastUpdate());
    assertNull(mappedArticleDto.getDateOfCreation());
  }
}
