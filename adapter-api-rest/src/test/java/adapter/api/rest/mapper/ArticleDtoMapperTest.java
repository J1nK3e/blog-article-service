package adapter.api.rest.mapper;

import adapter.persistence.mongodb.mapper.ArticleMapper;
import de.geldentdecker.domain.model.Article;
import lombok.val;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArticleDtoMapperTest {
  private static final String ARTICLE_ID = "32";
  private static final String ARTICLE_NAME = "ArticleName";
  private static final String ARTICLE_DATE = "28.12.1988";

  @Test
  public void testMappingFromArticleToArticleDto() {

    // given
    val article =
        Article.builder()
            .id(ARTICLE_ID)
            .name(ARTICLE_NAME)
            .lastUpdate(ARTICLE_DATE)
            .dateOfCreation(ARTICLE_DATE)
            .build();

    // when
    val articleMappedToArticleDto = ArticleMapper.INSTANCE.map(article);

    // then
    assertNotNull(articleMappedToArticleDto);
    assertNotNull(articleMappedToArticleDto.getId());
    assertEquals(ARTICLE_ID, articleMappedToArticleDto.getId());
    assertNotNull(articleMappedToArticleDto.getName());
    assertEquals(ARTICLE_NAME, articleMappedToArticleDto.getName());
    assertNotNull(articleMappedToArticleDto.getLastUpdate());
    assertEquals(ARTICLE_DATE, articleMappedToArticleDto.getLastUpdate());
    assertNotNull(articleMappedToArticleDto.getDateOfCreation());
    assertEquals(ARTICLE_DATE, articleMappedToArticleDto.getDateOfCreation());
  }
}
