package de.geldentdecker.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(
    scanBasePackages = {
      "adapter.api.rest",
      "adapter.persistence.mongodb",
      "de.geldentdecker.domain"
    })
@EnableMongoRepositories(basePackages = "adapter.persistence.mongodb")
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
