package de.geldentdecker.application;

import adapter.persistence.mongodb.MongoDbPersistenceImpl;
import com.tngtech.jgiven.integration.spring.SpringRuleScenarioTest;
import de.geldentdecker.application.jgiven.ComponentTestGiven;
import de.geldentdecker.application.jgiven.ComponentTestThen;
import de.geldentdecker.application.jgiven.ComponentTestWhen;
import de.geldentdecker.application.testdata.ArticleTestDataProvider;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;

@SpringBootTest(classes = {MockServletContext.class, ComponentTestContext.class})
public class ComponentTest
    extends SpringRuleScenarioTest<ComponentTestGiven, ComponentTestWhen, ComponentTestThen> {

  @Autowired private MongoDbPersistenceImpl mongoDbPersistence;

  @After
  public void tearDown() {
    deleteTestEntriesFromDatabase();
  }

  @Test
  public void articleEndpointShouldReturnAllArticles() {
    given().there_are_stored_articles_in_the_database();
    when().the_articles_endpoint_is_requested();
    then()
        .the_endpoint_returns_a_set_of_articles()
        .and()
        .the_length_of_article_set_is_bigger_then_$(1);
  }

  private void deleteTestEntriesFromDatabase() {
    ArticleTestDataProvider.getDummyArticleIds()
        .forEach(it -> mongoDbPersistence.removeArticle(it));
  }
}
