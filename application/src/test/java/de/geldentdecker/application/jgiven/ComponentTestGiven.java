package de.geldentdecker.application.jgiven;

import adapter.persistence.mongodb.MongoDbPersistenceImpl;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import de.geldentdecker.application.testdata.ArticleTestDataProvider;
import org.springframework.beans.factory.annotation.Autowired;

@JGivenStage
public class ComponentTestGiven extends Stage<ComponentTestGiven> {

  @Autowired MongoDbPersistenceImpl mongoDbPersistence;

  public ComponentTestGiven there_are_stored_articles_in_the_database() {
    ArticleTestDataProvider.buildDummyArticles().forEach(it -> mongoDbPersistence.writeArticle(it));
    return this;
  }
}
