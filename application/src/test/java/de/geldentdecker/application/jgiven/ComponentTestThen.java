package de.geldentdecker.application.jgiven;

import adapter.api.rest.model.ArticleDto;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ExpectedScenarioState;
import com.tngtech.jgiven.integration.spring.JGivenStage;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@JGivenStage
public class ComponentTestThen extends Stage<ComponentTestThen> {
  @ExpectedScenarioState Set<ArticleDto> response;

  public ComponentTestThen the_endpoint_returns_a_set_of_articles() {
    assertNotNull(response);
    return self();
  }

  public ComponentTestThen the_length_of_article_set_is_bigger_then_$(int length) {
    assertEquals(length, response.size());
    return self();
  }
}
