package de.geldentdecker.application.jgiven;

import adapter.api.rest.ArticleController;
import adapter.api.rest.model.ArticleDto;
import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;
import com.tngtech.jgiven.integration.spring.JGivenStage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

@JGivenStage
public class ComponentTestWhen extends Stage<ComponentTestWhen> {
  @ProvidedScenarioState Set<ArticleDto> response = new HashSet<>();

  @Autowired ArticleController articleController;

  public ComponentTestWhen the_articles_endpoint_is_requested() {
    response = articleController.readAllArticles();
    return self();
  }
}
