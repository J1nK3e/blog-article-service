package de.geldentdecker.application.adapter.persistence.mongodb.test;

import de.geldentdecker.application.Application;
import de.geldentdecker.domain.model.Article;
import de.geldentdecker.domain.service.ArticleService;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ComponentScan
@SpringBootTest(classes = Application.class)
public class test {

  @Autowired ArticleService articleService;

  @BeforeEach
  void addOneArticleToDatabase() {
    this.articleService.writeArticle(Article.builder().name("Kaller").id("d").build());
  }

  @Test
  public void countAllCountries() {
    val k = articleService.readAllArticles();
    int countries = 32;
  }
}
