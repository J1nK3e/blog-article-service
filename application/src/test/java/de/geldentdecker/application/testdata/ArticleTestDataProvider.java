package de.geldentdecker.application.testdata;

import de.geldentdecker.domain.model.Article;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class ArticleTestDataProvider {

  public static List<Article> buildDummyArticles() {
    return Collections.singletonList(
        Article.builder().id("32").name("name").lastUpdate("date").dateOfCreation("date").build());
  }

  public static List<String> getDummyArticleIds() {
    return buildDummyArticles().stream().map(Article::getId).collect(Collectors.toList());
  }
}
