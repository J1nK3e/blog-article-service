package de.geldentdecker.domain.logic;

import de.geldentdecker.domain.model.Article;
import de.geldentdecker.domain.service.ArticleService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Set;

@Component
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArticleHandler {

    @Autowired
    private ArticleService articleService;

    public Article buildArticle(Article article) {
        return Article.builder()
                .name(article.getName())
                .lastUpdate(Instant.now().toString())
                .dateOfCreation(Instant.now().toString())
                .build();
    }

    public Article saveArticle(Article article) {
        return articleService.writeArticle(article);
    }

    public Set<Article> readAllArticles() {
        return articleService.readAllArticles();
    }
}
