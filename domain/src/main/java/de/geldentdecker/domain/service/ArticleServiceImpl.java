package de.geldentdecker.domain.service;

import de.geldentdecker.domain.Persistence;
import de.geldentdecker.domain.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ArticleServiceImpl implements ArticleService {
  @Autowired Persistence persistence;

  @Override
  public Article writeArticle(Article article) {
    Article savedArticle = persistence.writeArticle(article);
    return savedArticle;
  }

  @Override
  public Set<Article> readAllArticles() {
    return persistence.readAllArticles();
  }
}
