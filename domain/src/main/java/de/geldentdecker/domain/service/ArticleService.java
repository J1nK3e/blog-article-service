package de.geldentdecker.domain.service;

import de.geldentdecker.domain.model.Article;

import java.util.Set;

public interface ArticleService {
  Article writeArticle(Article article);

  Set<Article> readAllArticles();
}
