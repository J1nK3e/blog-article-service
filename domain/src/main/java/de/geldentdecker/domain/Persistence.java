package de.geldentdecker.domain;

import de.geldentdecker.domain.model.Article;

import java.util.Set;

public interface Persistence {

  Article writeArticle(Article article);

  void removeArticle(String id);

  Set<Article> readAllArticles();
}
