package de.geldentdecker.domain.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Article {
    private String id;
    @NonNull
    private String name;
    private String lastUpdate;
    private String dateOfCreation;
}
