package adapter.persistence.mongodb.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Article")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ArticleForDatabase {
  private @Id String id;
  private @NonNull String name;
  private String lastUpdate;
  private String dateOfCreation;
}
