package adapter.persistence.mongodb;

import adapter.persistence.mongodb.mapper.ArticleMapper;
import adapter.persistence.mongodb.model.ArticleForDatabase;
import adapter.persistence.mongodb.repositorys.ArticleRepository;
import de.geldentdecker.domain.Persistence;
import de.geldentdecker.domain.model.Article;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Builder
@AllArgsConstructor
public class MongoDbPersistenceImpl implements Persistence {

  @Autowired private final ArticleRepository articleRepository;

  @Override
  public Article writeArticle(Article article) {
    Optional<ArticleForDatabase> storedArticle =
        Optional.of(articleRepository.save(ArticleMapper.INSTANCE.map(article)));
    if (storedArticle.isPresent()) {
      return ArticleMapper.INSTANCE.map(storedArticle.get());
    }
    return Article.builder().id(null).build();
  }

  @Override
  public void removeArticle(String id) {
    articleRepository.deleteById(id);
  }

  @Override
  public Set<Article> readAllArticles() {
    return articleRepository.findAll().stream()
        .map(ArticleMapper.INSTANCE::map)
        .collect(Collectors.toSet());
  }
}
