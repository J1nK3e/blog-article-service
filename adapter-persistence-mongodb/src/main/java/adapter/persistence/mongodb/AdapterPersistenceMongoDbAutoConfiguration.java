package adapter.persistence.mongodb;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class AdapterPersistenceMongoDbAutoConfiguration {}
