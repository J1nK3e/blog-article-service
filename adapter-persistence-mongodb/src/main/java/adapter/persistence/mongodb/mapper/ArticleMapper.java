package adapter.persistence.mongodb.mapper;

import adapter.persistence.mongodb.model.ArticleForDatabase;
import de.geldentdecker.domain.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper()
public abstract class ArticleMapper {

  public static final ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);

  public abstract Article map(ArticleForDatabase articleForDatabase);

  public abstract ArticleForDatabase map(Article article);
}
