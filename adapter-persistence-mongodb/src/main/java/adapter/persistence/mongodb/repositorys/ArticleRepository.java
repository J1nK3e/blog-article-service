package adapter.persistence.mongodb.repositorys;

import adapter.persistence.mongodb.model.ArticleForDatabase;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface ArticleRepository extends MongoRepository<ArticleForDatabase, String> {}
