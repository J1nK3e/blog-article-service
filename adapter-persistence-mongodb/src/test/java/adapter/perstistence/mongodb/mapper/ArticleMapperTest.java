package adapter.perstistence.mongodb.mapper;

import adapter.persistence.mongodb.mapper.ArticleMapper;
import adapter.persistence.mongodb.model.ArticleForDatabase;
import lombok.val;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArticleMapperTest {

  private static final String Id = "12345679";
  private static final String ARTICLE_NAME = "ArticleName";
  private static final String INSTANT_FOR_UPDATE_AND_DATE_OF_CREATION = Instant.now().toString();

  @Test
  public void testMappingFromArticleDtoToArticle() {

    // given
    val articleDto =
        ArticleForDatabase.builder()
            .id(Id)
            .name(ARTICLE_NAME)
            .lastUpdate(INSTANT_FOR_UPDATE_AND_DATE_OF_CREATION)
            .dateOfCreation(INSTANT_FOR_UPDATE_AND_DATE_OF_CREATION)
            .build();

    // when
    val mappedArticleDto = ArticleMapper.INSTANCE.map(articleDto);

    // then
    assertNotNull(mappedArticleDto);
    assertNotNull(mappedArticleDto.getId());
    assertEquals(mappedArticleDto.getId(), Id);
    assertNotNull(mappedArticleDto.getName());
    assertEquals(mappedArticleDto.getName(), ARTICLE_NAME);
    assertNotNull(mappedArticleDto.getLastUpdate());
    assertEquals(mappedArticleDto.getLastUpdate(), INSTANT_FOR_UPDATE_AND_DATE_OF_CREATION);
    assertNotNull(mappedArticleDto.getDateOfCreation());
    assertEquals(mappedArticleDto.getDateOfCreation(), INSTANT_FOR_UPDATE_AND_DATE_OF_CREATION);
  }
}
